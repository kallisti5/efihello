#include <efi/types.h>
#include <efi/system-table.h>
#include <efi/protocol/simple-text-input.h>

efi_system_table* gSystemTable;
 
efi_status efi_main(efi_handle ImageHandle, efi_system_table *SystemTable)
{
    efi_status Status;
    efi_input_key Key;
 
    /* Store the system table for future use in other functions */
    gSystemTable = SystemTable;
 
    /* Say hi */
    Status = gSystemTable->ConOut->OutputString(gSystemTable->ConOut, L"Hello World\n\r");
    if (EFI_ERROR(Status))
        return Status;
 
    /* Now wait for a keystroke before continuing, otherwise your
       message will flash off the screen before you see it.
 
       First, we need to empty the console input buffer to flush
       out any keystrokes entered before this point */
    Status = gSystemTable->ConIn->Reset(gSystemTable->ConIn, 0);
    if (EFI_ERROR(Status))
        return Status;
 
    /* Now wait until a key becomes available.  This is a simple
       polling implementation.  You could try and use the WaitForKey
       event instead if you like */
    while ((Status = gSystemTable->ConIn->ReadKeyStroke(gSystemTable->ConIn, &Key)) == EFI_NOT_READY) ;
 
    return Status;
}

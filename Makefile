HAIKU_SRC ?= ~/Code/haiku
PREFIX ?= $(HAIKU_SRC)/generated.arm/cross-tools-arm/bin/arm-unknown-haiku-
LIBGCC = $(HAIKU_SRC)/generated.arm/cross-tools-arm/lib/gcc/arm-unknown-haiku/8.3.0/libgcc.a
CC = $(PREFIX)gcc
OBJCOPY = $(PREFIX)objcopy
LD = $(PREFIX)ld

default:
	# Build sources
	$(CC) -ffreestanding -I $(HAIKU_SRC)/headers/private/kernel/platform -I $(HAIKU_SRC)/headers/posix/ -I $(HAIKU_SRC)/headers/ -c -o hello.o hello.c
	$(CC) -ffreestanding -c -o crt0-efi-arm.o $(HAIKU_SRC)/src/system/boot/platform/efi/arch/arm/crt0-efi-arm.S
	$(CC) -ffreestanding -I $(HAIKU_SRC)/headers/private/kernel/platform -I $(HAIKU_SRC)/headers/posix/ -I $(HAIKU_SRC)/headers/ \
		-I $(HAIKU_SRC)/headers/private/kernel/arch -I $(HAIKU_SRC)/headers/private/system -I $(HAIKU_SRC)/headers/os \
		-I $(HAIKU_SRC)/headers/private/system/arch/arm/ -I $(HAIKU_SRC)/headers/os/support \
		-c -o relocation.o $(HAIKU_SRC)/src/system/boot/platform/efi/arch/arm/relocation_func.cpp
	# Link gnuefi-style
	$(CC) -nostdlib -Wl,-dll -shared -T $(HAIKU_SRC)/src/system/ldscripts/arm/boot_loader_efi.ld -o raw_loader_gnuefi crt0-efi-arm.o hello.o relocation.o $(LIBGCC)
	$(OBJCOPY) -j .text -j .sdata -j .data -j .dynamic -j .dynsym -j .rel -j .rela -j .reloc -j .dynstr --output-target=binary raw_loader_gnuefi loader.efi
	# Link elf2efi32 style
	$(LD) -nostdlib -static -T lds/efi.lds -o raw_loader_elf2efi32 hello.o relocation.o $(LIBGCC)
	elf2efi32 raw_loader_elf2efi32 loader_alt.efi

clean:
	rm -f *.o *.efi raw_loader*
